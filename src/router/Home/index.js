export default {
  path: '/home',
  name: 'home',
  component: resolve => require(['./View.vue'], resolve),
  meta: {
    auth: true
  },
  props: {
    title: 'Home'
  }
};
