const homeModuleStore = {
  state: {
    todos: [
      {label: 'Learning Vue', checked: true},
      {label: 'Learning Vuex', checked: false},
      {label: 'Creating an app', checked: false}
    ]
  },
  getters: {
    todos: state => state.todos
  },
  mutations: {
    createTodo(state, todo) {
      state.todos.push(todo);
    },
    checkTodo(state, index) {
      let todo = state.todos[index];
      todo.checked = !todo.checked;
    },
    removeTodo(state,  index) {
      state.todos.splice(index, 1);
    }
  }
};

export default homeModuleStore;
