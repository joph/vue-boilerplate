import Vue from 'vue';
import { sync } from 'vuex-router-sync';
import VueRouter from 'vue-router';
import store  from 'store';
import routes from './routes';

// Adding complementary library for managment routes in vue.js
Vue.use(VueRouter);
// Create a router
const router = new VueRouter({routes});
// Sync vuex state with the router's data
sync(store, router);

router.beforeEach((to, from, next) => {
  const auth = to.matched.some(route => route.meta.auth);
  const userLogged = store.getters.isLogged;
  
  // validates if the user has been logged and can access to restricted routes
  if(auth && !userLogged) {
    next('/login');
  } else if(to.name === 'login' && userLogged) {
    next('home');
  } else {
    next();
  }
});

export default router;
