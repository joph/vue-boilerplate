export default {
  path: '/view-1',
  name: 'view1',
  component: resolve => require(['./View.vue'], resolve),
  meta: {
    auth: true
  },
  props: {
    title: 'View 1'
  }
};
