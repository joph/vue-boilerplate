import state from './state';
import getters from './getters';
import mutations from './mutations';

const editorModule = {
  state,
  getters,
  mutations
};

export default editorModule;