export default {
  path: '/login',
  name: 'login',
  component: resolve => require(['./View.vue'], resolve),
  props: {
    title: 'Login'
  }
};
