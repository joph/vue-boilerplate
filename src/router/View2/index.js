export default {
  path: '/view-2',
  name: 'view2',
  component: resolve => require(['./View.vue'], resolve),
  meta: {
    auth: true
  },
  props: {
    title: 'View 2'
  }
};
