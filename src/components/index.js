import camelCase from 'lodash/camelCase';

// Bundles all components in one module
let context = require.context('./', true, /(([a-z])\w+)\/index.vue$/g);
let compFiles = context.keys();

compFiles.forEach(pathFile => {
  var compName = pathFile.split('/')[1];
  compName = camelCase(compName);
  compName = compName.charAt(0).toUpperCase() + compName.slice(1);

  // Here saves and exports the components like a separate export, not as default
  exports[compName] = context(pathFile);
});
