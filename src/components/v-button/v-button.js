import LinkTo from 'components/link-to';

export default {
  data () {
    return {};
  },
  props: {
    label: String,
    link: String,
    onClick: Function
  },
  components: {
    LinkTo
  }
}