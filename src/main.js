import Vue    from 'vue';
import Vuex   from 'vuex';
import router from 'router';
import store  from './store';
import App    from './App';

// import all global CSS styles
import 'styles/main.styl';

// Get ready to rock!!!!
new Vue({
  ...App,
  router,
  store,
  el: '#app',
});
