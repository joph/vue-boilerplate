import Vue from 'vue';
import Vuex from 'vuex';

// Adding managment state library
Vue.use(Vuex);

const userMock = {
  username: 'foo',
  password: 'bar'
}

const state = {
  app: {
    userIsLogged: false
  },
  routeStates: {}
}

const getters = {
  isLogged: state => state.app.userIsLogged,
};

const mutations = {
  validateLogin(state, { username, password }) {
    if(username === userMock.username && password === userMock.password) {
      state.app.userIsLogged = true;
    }
  }
};

const actions = {
  login({ commit, state }, payload) {
    return new Promise((resolve, reject) => {
      commit('validateLogin', payload);
      
      if(state.app.userIsLogged) {
        resolve();
      } else {
        reject('Wrong Credentials');
      }
    });
  }
}

const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions
});

export default store;