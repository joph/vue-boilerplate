const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: [
    './src/main.js',
  ],
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src', 'index.jade'),
      filename: 'index.html',
      inject: true
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  ],
  resolve: {
    extensions: ['.js', '.vue'],
    // makes the main routes absolute for the whole project
    alias: {
      router: path.resolve(__dirname, 'src/router'),
      components: path.resolve(__dirname, 'src/components'),
      store: path.resolve(__dirname, 'src/store'),
      utils: path.resolve(__dirname, 'src/utils'),
      styles: path.resolve(__dirname, 'src/styles')
    }
  },
  module: {
    loaders: [
      { test: /\.vue?$/, loader: 'vue-loader' },
      { test: /\.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      { test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader' },
      { test: /\.jade?$/, loader: 'jade-loader' },
      {
        test: /\.(jpg|png|eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader'
      },
      {
        test: /\.(gif|mp3|mp4|webm)/,
        loader: 'file-loader'
      }
    ],
  },
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: 'bundle.js',
  },
  devServer: {
    hot: true,
  },
};
